# NewsBlitz Mobile App

This project is generated with Ionic generator

## Build & development

Run `ionic build` for building and `ionic serve --lab` for preview. 

To deploy to an Android mobile device, connect device to PC and run `ionic run android`

To directly deploy app to an Android mobile device, copy the appropriate apk file from the apk folder