angular.module('newsblitz.controllers', [])

  .controller('AppCtrl', function ($scope, $ionicModal, $timeout, loginService, registrationService, $localStorage, $state, $cordovaToast, $cordovaDialogs, $cordovaNetwork) {

    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //$scope.$on('$ionicView.enter', function(e) {
    //});

    document.addEventListener("deviceready", function () {

      var isOffline = $cordovaNetwork.isOffline();

      if (isOffline) {

        $cordovaToast.showLongBottom('Please check your internet connection').then(function (success) {
          // success
        }, function (error) {
          // error
        });


      }


    });
    // Form data for the login modal
    $scope.loginData = {};

    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.loginModal = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeLogin = function () {
      $scope.loginModal.hide();
      $state.go('app.welcome');
    };

    // Open the login modal
    $scope.login = function () {
      $scope.loginModal.show();
    };

    // Perform the login action when the user submits the login form
    $scope.doLogin = function () {

      loginService.loginUser($scope.loginData.email, $scope.loginData.password);
      $scope.closeLogin();


    };

    // Open the login modal
    $scope.logout = function () {

      $cordovaDialogs.confirm('Are you sure you want to log out ?', 'Logout', ['Yes', 'No'])
        .then(function (buttonIndex) {
          // no button = 0, 'OK' = 1, 'Cancel' = 2
          var btnIndex = buttonIndex;

          if (btnIndex === 1) {
            $localStorage.removeObject("userObj");
            $scope.loginModal.show();


          }

        });
    };


    $scope.registrationData = {};

    // Create the Register modal that we will use later
    $ionicModal.fromTemplateUrl('templates/register.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.registerModal = modal;
    });

    // Triggered in the Register modal to close it
    $scope.closeRegister = function () {
      $scope.registerModal.hide();
    };

    // Open the Register modal
    $scope.register = function () {
      $scope.registerModal.show();
    };

    $scope.doRegister = function () {
      registrationService.registerUser($scope.registrationData);
      $scope.closeRegister();
      $scope.login();

    };


  })


  //controller for loading news articles in home page
  .controller('HomeController', ['$scope', 'NewsLoadService', '$state', '$localStorage', '$cordovaToast', 'networkCheckService', function ($scope, NewsLoadService, $state, $localStorage, $cordovaToast, networkCheckService) {

    //get user object to load sources
    $scope.getUserInfo = function () {

      networkCheckService.isOffline();

      if ((typeof $localStorage.get("userObj")) != 'undefined') {
        $scope.userObj = $localStorage.getObject("userObj");

        if ($scope.userObj.subscriptions[0].id === null) {

          $cordovaToast.showLongBottom('You have no Subscriptions. Add a few').then(function (success) {
            // success
          }, function (error) {
            // error
          });

          $state.go('app.subscriptions');
        }
      }



      else {
        $state.go('app.welcome');
      }

      $scope.userSubscriptions = [];
      for (var i = 0, len = $scope.userObj.subscriptions.length; i < len; i++) {

        if ($scope.userObj.subscriptions[i].id !== null && $scope.userObj.subscriptions[i].id !== undefined && $scope.userObj.subscriptions[i].id.length > 3) {

          $scope.userSubscriptions.push($scope.userObj.subscriptions[i]);

        }
      }


    };

    //load image and  headline in home page
    $scope.getImageAndHeadline = function (source) {

      var myDataPromise = NewsLoadService.loadNewsDeferred(source.id);


      myDataPromise.then(function () {

        var newsItem = $localStorage.getObject(source.id)[0];

        document.getElementById(source.id + "_image").src = newsItem.urlToImage;
        document.getElementById(source.id + "_title").innerText = newsItem.title;
        document.getElementById(source.id + "_description").innerText = newsItem.description;

      });


    };

    $scope.goToDetail = function (source) {


      $state.go('app.detail', { "source": source.id });

    };


  }])


  .controller('DetailController', ['$scope', '$localStorage', '$state', 'networkCheckService', function ($scope, $localStorage, $state, networkCheckService) {

    //load all articles in a news source in detail page
    $scope.loadArticles = function () {

      networkCheckService.isOffline();

      var source = $state.params.source;

      var src = source.replace(":", "");

      $scope.userObj = $localStorage.getObject("userObj");

      $scope.articles = $localStorage.getObject(src);

    };

  }])

  //manage adding/removing subscriptions
  .controller('SubscriptionsController', ['$scope', 'SubscriptionService', '$localStorage', 'networkCheckService', function ($scope, SubscriptionService, $localStorage, networkCheckService) {

    $scope.loadSubscriptions = function () {

      networkCheckService.isOffline();

      var myDataPromise = SubscriptionService.loadNewsSourcesDeferred();

      myDataPromise.then(function (result) {


        $scope.sources = $localStorage.getObject("AllSources");

      });
    };

    //set the checked/unchecked status of checkboxes in subscriptions page
    $scope.setStatus = function (id) {

      $scope.userSources = $localStorage.getObject("userObj").subscriptions;

      for (var i = 0, len = $scope.userSources.length; i < len; i++) {
        if (document.getElementById(id) !== null && $scope.userSources[i].id === id) {

          return true;
        }

      }
      return false;

    };

    $scope.updateSubscription = function (id, name) {

      networkCheckService.isOffline();

      if (document.getElementById(id).checked === true) {
        SubscriptionService.addSource(id, name);
      }
      else {
        SubscriptionService.removeSource(id);
      }
    };

  }])


  ;



