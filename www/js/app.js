// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('newsblitz', ['ionic', 'ngCordova', 'newsblitz.controllers', 'newsblitz.services'])
  .constant("baseURL", "https://newsblitz-db-testing.mybluemix.net/api/usersinfo")
  .constant("newsSourcesURL", "https://newsapi.org/v1/sources?language=en")
  .constant("newsArticlesBaseURL", "https://newsapi.org/v1/articles?source={source_name}&apiKey=ecf0d39691f5462991b503dfcdf0a148")


  .run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
    });
  })

  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider

      .state('app.welcome', {
        url: '/welcome',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/welcome.html',
            controller: 'AppCtrl'
          }
        }
      })

      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
      })

      .state('app.subscriptions', {
        url: '/subscriptions',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/subscriptions.html',
            controller: 'SubscriptionsController'
          }
        }
      })

      
      .state('app.detail', {
        url: '/detail',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/detail.html',
            controller: 'DetailController'
          }
        },
        params: {
          "source": "default_source"
        }
      })

      .state('app.home', {
        url: '/home',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'templates/home.html',
            controller: 'HomeController'
          }
        }
      });
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/welcome');
  });
