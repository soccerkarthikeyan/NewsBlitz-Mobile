'use strict';
angular.module('newsblitz.services', [])

    //check network status
    .service('networkCheckService', ['$cordovaNetwork', '$cordovaToast', function ($cordovaNetwork, $cordovaToast) {

        this.isOffline = function () {

            var isOffline = $cordovaNetwork.isOffline();

            if (isOffline) {

                $cordovaToast.showLongBottom('Please check your internet connection').then(function (success) {
                    // success
                }, function (error) {
                    // error
                });


            }
        };

    }])

    //service to register users
    .service('registrationService', ['$http', '$state', 'baseURL', '$cordovaToast', function ($http, $state, baseURL, $cordovaToast) {


        this.registerUser = function (registrationData) {

            var requestObj = {};

            var subscriptions = [];

            subscriptions.push({ "id": null, "name": null });

            requestObj.firstname = registrationData.firstname;
            requestObj.lastname = registrationData.lastname;
            requestObj.email = registrationData.email;
            requestObj.password = registrationData.password;
            requestObj.subscriptions = subscriptions;

            var request = JSON.stringify(requestObj);

            $http({
                method: "POST",
                url: baseURL,
                data: request
            }).then(function mySucces(response) {
                $cordovaToast.showLongBottom('Registered Successfully').then(function (success) {
                    // success
                }, function (error) {
                    // error
                });

            }, function myError(response) {
                $cordovaToast.showLongBottom('Registration Unsucessful. Please complete all fields and try again').then(function (success) {
                    // success
                }, function (error) {
                    // error
                });

                console.log(response.statusText);
            });

        };


    }])

    //user login service
    .service('loginService', ['$http', '$localStorage', 'baseURL', '$cordovaToast', '$state', function ($http, $localStorage, baseURL, $cordovaToast, $state) {


        this.loginUser = function (inputEmail, password) {

            $http({
                method: "GET",
                url: baseURL + "?filter[where][and][0][email]=" + inputEmail + "&filter[and][1][password]=" + password
            }).then(function mySucces(response) {

                if (response.data.length > 0) {
                    $localStorage.storeObject("userObj", response.data[0]);
                    $cordovaToast.showLongBottom('Log in Successful').then(function (success) {
                        $state.go('app.home');
                        // success
                    }, function (error) {
                        // error
                    });

                }

                else {
                    $cordovaToast.showLongBottom('Invalid email/password. Please try again').then(function (success) {
                        // success
                    }, function (error) {
                        // error
                    });
                    console.log(response.data);
                }

            }, function myError(response) {
                $cordovaToast.showLongBottom('Invalid email/password. Please try again').then(function (success) {
                    // success
                }, function (error) {
                    // error
                });
                console.log(response.statusText);
            });

        };


    }])


    // serivice to manage subscriptions
    .service('SubscriptionService', ['$http', '$localStorage', 'baseURL', 'newsSourcesURL', '$cordovaToast', function ($http, $localStorage, baseURL, newsSourcesURL, $cordovaToast) {

        this.loadNewsSourcesDeferred = function () {

            return $http({
                method: "GET",
                url: newsSourcesURL
            }).then(function mySucces(response) {
                $localStorage.storeObject("AllSources", response.data.sources);
                return response.data.sources;
            }, function myError(response) {
                console.log(response.statusText);
            });


        };


        //add subscription
        this.addSource = function (id, name) {

            var userObj = $localStorage.getObject("userObj");

            var requestObj = userObj;

            var subscriptions = [];

            var subscription = {};

            subscription.id = id;
            subscription.name = name;

            subscriptions.push(subscription);

            if (requestObj.subscriptions[0].id === null) {
                requestObj.subscriptions = subscriptions;
            }

            else {
                requestObj.subscriptions.push(subscription);
            }
            $http({
                method: "PUT",
                url: baseURL + "/" + userObj.id,
                data: requestObj
            }).then(function mySucces(response) {
                $cordovaToast.showLongBottom('Subscription added').then(function (success) {
                    // success
                }, function (error) {
                    // error
                });
                $localStorage.storeObject("userObj", response.data);

            }, function myError(response) {
                console.log(response.statusText);
            });



        };

        //remove subscription
        this.removeSource = function (id) {

            var userObj = $localStorage.getObject("userObj");

            var requestObj = userObj;

            for (var i = 0, len = userObj.subscriptions.length; i < len; i++) {

                if (id === userObj.subscriptions[i].id) {
                    delete requestObj.subscriptions[i];
                }
            }

            $http({
                method: "PUT",
                url: baseURL + "/" + userObj.id,
                data: requestObj
            }).then(function mySucces(response) {
                $cordovaToast.showLongBottom('Unsubscribed Successfully').then(function (success) {
                    // success
                }, function (error) {
                    // error
                });
                $localStorage.storeObject("userObj", response.data);

            }, function myError(response) {
                console.log(response.statusText);
            });



        };


    }])

    //load news articles
    .service('NewsLoadService', ['$http', '$localStorage', 'newsArticlesBaseURL', function ($http, $localStorage, newsArticlesBaseURL) {

        this.loadNewsDeferred = function (id) {

            var url = newsArticlesBaseURL.replace("{source_name}", id);
            return $http({
                method: "GET",
                url: url
            }).then(function mySucces(response) {
                $localStorage.storeObject(id, response.data.articles);
                return response.data.articles;
            }, function myError(response) {
                console.log(response.statusText);
            });


        };


    }])

    // local storage
    .factory('$localStorage', ['$window', function ($window) {
        return {
            store: function (key, value) {
                $window.localStorage[key] = value;
            },
            get: function (key, defaultValue) {
                return $window.localStorage[key] || defaultValue;
            },
            storeObject: function (key, value) {
                $window.localStorage[key] = JSON.stringify(value);
            },
            getObject: function (key, defaultValue) {
                return JSON.parse($window.localStorage[key] || defaultValue);
            },
            removeObject: function (key, index) {
                $window.localStorage.removeItem(key);
                //$window.localStorage[key] = $window.localStorage[key].splice(index, 1);
            }
        }
    }])

    ;
